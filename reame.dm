Crear una página Web que permita ejecutar y mostrar:
1. •Transforme la hora actual en segundos.
2. •Calcular el área de un triángulo, que es igual a: (base * altura) /2.
3. •Calcule la raíz cuadrada de un número impar y muestre el resultado con 3 
dígitos.
4. •Ingresar una cadena de texto y mostrar la longitud de la cadena.
5. •Concatenar los arrays: array1(Lunes, Martes, Miércoles, Jueves, Viernes) y 
array 2 (Sábado, Domingo)
6. •Mostrar la versión del navegador.
7. •Mostrar el ancho y la altura de la pantalla.
8. •Imprimir la página